En el curso realizado se encuentran los cambios mas importantes que se implementaron con la llegada de Java 8. 
Los temas que se ven durante el curso son:
	-Expresiones lamdas
	-Optional
	-Referencia de Metodos
	-Default Methods
	-Streams
	-Metodos de ordenamiento en Collections o Listas
	-Scope de las variables en Expresiones lamdas.
	
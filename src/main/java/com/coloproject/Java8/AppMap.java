package com.coloproject.Java8;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class AppMap {
	
	private Map<Integer,String> map;
	
	public void poblar() {
		map = new HashMap<>();
		map.put(1, "Lucas");
		map.put(2, "Leonel");
		map.put(3, "Galetti");
		map.put(4, "Colo");
	}
	
	public void imprimir_v7() {
		for (Entry<Integer,String> e : map.entrySet()) {
			System.out.println("Llave: " + e.getKey() + "Valor: " + e.getValue());
		}
	}
	
	public void impimir_v8() {
//		map.forEach((k,v) -> System.out.println("Llave: " + k + " Valor: "+ v));
		map.entrySet().stream().forEach(System.out::println);
	}
	
	public void insertarSiAusente() {
		map.putIfAbsent(5, "FullyDev");
	}
	
	public void operarSiPresente() {
		map.computeIfPresent(3, (k,v) -> k + v);
		System.out.println(map.get(3));
	}
	
	public void obtenerOrPredeterminado() {
		String valor = map.getOrDefault(5, "Valor por defecto");
		System.out.println(valor);
	}
	
	public void recolectar() {
		Map<Integer,String> mapaRecolectado = map.entrySet().stream()
												.filter(e -> e.getValue().contains("Sus"))
												.collect(Collectors.toMap(p -> p.getKey(),p-> p.getValue()));
		mapaRecolectado.forEach((k,v) -> System.out.println("Llave " + k + " Valor: " + v));
	}
	
	
	public static void main(String[] args) {
		AppMap app = new AppMap();
		app.poblar();
		app.imprimir_v7();
		app.impimir_v8();
		app.operarSiPresente();
		app.insertarSiAusente();
		app.recolectar();
		
	}
		
}

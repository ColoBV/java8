package com.coloproject.Java8;

import com.coloproject.Java8.interfaces.Operacion;

public class FuncInterApp {

	public double operar(double x, double y) {
		Operacion op = (n1,n2) -> n1 + n2;
		return op.calcular(x, y);
	}
	
	public static void main(String[] args) {
		FuncInterApp app = new FuncInterApp();
		double rpta = app.operar(4, 5);
		System.out.println(rpta);
	}
	
}

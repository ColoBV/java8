package com.coloproject.Java8;

import java.util.ArrayList;
import java.util.List;

public class StreamsApp {
	private List<String> lista;
	private List<String> numeros;
	
	
	public StreamsApp() {
		lista = new ArrayList<>();
		lista.add("Michel");
		lista.add("Tito");
		lista.add("Gordo");
		lista.add("Cabezon");
		
		numeros.add("1");
		numeros.add("2");
	}
	
	public void filtrar() {
				//el print se puede reemplazar por lamda: x-> System.out.println(x)
		lista.stream().filter(x -> x.startsWith("M")).forEach(System.out::println); 
	}
	public void ordenar() {
//		lista.stream().sorted().forEachOrdered(System.out::println);
		lista.stream().sorted((x,y)->y.compareTo(x)).forEachOrdered(System.out::println);
	}
	public void transformar() {
		lista.stream().map(String::toUpperCase).forEach(System.out::println);
		numeros.stream().map(x -> Integer.parseInt(x) + 1).forEach(System.out::println);
	}
	public void limitar() {
		lista.stream().limit(2).forEach(System.out::println);
	}
	public void contar() {
		long x = lista.stream().count();
		System.out.println(x);
	}
	public static void main(String[] args) {
		StreamsApp app = new StreamsApp();
		app.filtrar();
		app.transformar();
		app.contar();
	}
}

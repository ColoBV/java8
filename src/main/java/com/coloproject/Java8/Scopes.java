package com.coloproject.Java8;

import com.coloproject.Java8.interfaces.Operaciones;

public class Scopes {
	
	private static double atriv1;
	private double atriv2;
	
	public double probarLocalVariable() {
		//Es local a probarlocalvariable pero no local a calcularPromedio(). Por eso tiene que llevar el final
		final double n3 = 3;
		//Metodo declarativo.
		/*Operaciones op = new Operaciones() {
			
			@Override
			public double calcularPromedio(double n1, double n2) {
				return n3 + n1 + n2;
			}
		};
		return op.calcularPromedio(4, 3);*/
		//Mismo metodo pero con Lamdas
		Operaciones operacion = (x,y) -> {
			return x + y;
		};
		return operacion.calcularPromedio(4, 3);
	}

	public double probarAtributosStaticVariables() {
		Operaciones op = (x,y) ->{
			//Esto nos demuestra que en una expresion lamda me permite la lectoescritura de los atributos de una clase. 
			atriv1 = x + y;
			atriv2 = atriv1;
			return atriv2; 
		};
		return op.calcularPromedio(4, 4);
	}
	
	public static void main(String[] args) { 
		Scopes app = new Scopes();
		System.out.println(app.probarLocalVariable());
		System.out.println(app.probarAtributosStaticVariables());
	}
}

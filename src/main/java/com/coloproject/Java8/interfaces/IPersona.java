package com.coloproject.Java8.interfaces;

import com.coloproject.Java8.model.Persona;

@FunctionalInterface
public interface IPersona {
	Persona crear(int id,String nombre);
}

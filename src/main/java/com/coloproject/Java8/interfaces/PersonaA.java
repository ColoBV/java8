package com.coloproject.Java8.interfaces;

public interface PersonaA {
	public void caminar();
	
	// Se utiliza esto cuando necesito que un metodo tenga toda la funcionalidad por default.
	// Si no, tendremos que agregarlo acá e implementarlo en cada una de las clases que use la intefaz.
	default public void hablar() {
		System.out.println("Hola Amigueros - Persona A");
	}
}

package com.coloproject.Java8.interfaces;

@FunctionalInterface
public interface Operacion {
	double calcular(double n1,double n2);
}

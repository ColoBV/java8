package com.coloproject.Java8.interfaces;

// Se denomina interfaz funcional a aquella que solo apunta a una sola funcion.
// Si le llego a declarar mas de un tema rompe.

public interface Operaciones {
	double calcularPromedio(double n1,double n2);
//	double calcularPromedio();
//	double calcular(double n1,double n2);
	
}

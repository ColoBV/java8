package com.coloproject.Java8;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectionApp {
	
	private List<String> lista;
	
	public void llenarLista() {
		
		lista = new ArrayList<>();
		lista.add("Roberto");
		lista.add("Carlos");
		lista.add("Jose");
		
	}
	
	public void usarForEach() {
//		for (String elemento : lista) {
//			 System.out.println(elemento);
//		}
//		lista.forEach(x -> System.out.println(x));
		lista.forEach(System.out::println);
	}
	
	public void usarRemoveIf() {
		 
		//esto chingaba error se cambiaba por Iterator
//		Iterator<String> it =  lista.iterator();
//		for (String elemento : lista) {
//		while(it.hasNext()) {
//			if(it.next().equalsIgnoreCase("Carlos"))
//				it.remove();
//		}
		
		lista.removeIf(x -> x.equals("Code"));
	}
	
	public void usarSort() {
		lista.sort((x,y) -> x.compareTo(y));
	}
	
	public static void main(String[] args) {
		CollectionApp app = new CollectionApp();
		app.llenarLista();
		app.usarRemoveIf();
		app.usarForEach();
		app.usarSort();
	}
}

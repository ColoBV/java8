package com.coloproject.Java8;

import com.coloproject.Java8.interfaces.PersonaA;
import com.coloproject.Java8.interfaces.PersonaB;

public class DefaultMethod implements PersonaA,PersonaB {
	
	@Override
	public void caminar() {
		// TODO Auto-generated method stub
		
	}
	//El Ide te tira error al implementar las 2 interfaces y te pregunta cual de las 2 queres sobre-escribir.
	//Tambien podemos sobre-escribir con lo que queramos.
	@Override
	public void hablar() {
		//PersonaA.super.hablar();
		//Implemente las 2 puedo usar el metodo hablar pero voy a hacer lo que quiera.
		System.out.println("Hola Amigueros - DefaultMethod");
	}
	
	public static void main(String[] args) {
		DefaultMethod app = new DefaultMethod();
		app.hablar();
	}
	
}

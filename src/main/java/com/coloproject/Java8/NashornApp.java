package com.coloproject.Java8;

import java.io.FileReader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/*
 * Como parte final sobre éste tema, con jjs se puede ejecutar codigo de Java. 
 */

public class NashornApp {
	
	private ScriptEngineManager m;
	private ScriptEngine e;
	private Invocable invocador; // para invocar archivos js.
	
	public NashornApp() {
		m = new ScriptEngineManager();
		e = m.getEngineByName("nashorn");
		invocador = (Invocable)e; //Si no declado esto tira nullpointerexception
	}
	
	public void llamarFunciones() throws Exception {
		//No se puede agregar sintaxis para la manipulacion de DOM. Ni tampoco el tema de alerts. Solo codigo puro js
//		e.eval("print('JS desde Java')");
		e.eval(new FileReader("src/main/java/com/coloproject/Java8/js/archivo.js"));
		String rtpa = (String)invocador.invokeFunction("calcular", "2","3");
		System.out.println(rtpa);
		
		double rtpa2  = (double)invocador.invokeFunction("calcular", 2,3);
		System.out.println(rtpa);
		
	}
	
	public void implementarInterfaz() throws Exception{
		e.eval(new FileReader("src/main/java/com/coloproject/Java8/js/archivo.js"));
		Object implementacion = e.get("hiloImpl");
		Runnable r = invocador.getInterface(implementacion,Runnable.class);
		
		Thread th1 = new Thread(r);
		th1.start();
		
		Thread th2 = new Thread(r);
		th2.start();
	}
	
	public static void main(String[] args) throws Exception {
		NashornApp app = new NashornApp();
//		app.llamarFunciones();
		app.implementarInterfaz();
	}
	
}

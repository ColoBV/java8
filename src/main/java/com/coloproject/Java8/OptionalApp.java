package com.coloproject.Java8;

import java.util.Optional;

public class OptionalApp {
	
	public void probar(String valor) {
//		System.out.println(valor.contains("mito"));
		try {
			Optional op = Optional.empty();
			op.get();			
		} catch (Exception e) {
			System.out.println("No hay elemento");
		}
	}
	
	//Lo que hace este metodo es que si es vacio, le da un valor por defecto.
	public void orElse(String valor) {
		Optional<String> op = Optional.ofNullable(valor);
		String x = op.orElse("Predeterminado");
		System.out.println(x);
	}
	//Permite tirar una excepcion si hay un null en el objeto
	public void orElseThrow(String valor) {
		Optional<String> op = Optional.ofNullable(valor);
		op.orElseThrow(NumberFormatException::new);
	}
	//Permite saber si el valor fue inicializado o no.
	public void isPresent(String valor) {
		Optional<String> op = Optional.ofNullable(valor);
		System.out.println(op.isPresent());
	}
	public static void main(String[] args) {
		OptionalApp app = new OptionalApp();
		app.probar("mito");
		app.orElse(null);
		app.isPresent(null);
		app.orElseThrow(null);
	}
}

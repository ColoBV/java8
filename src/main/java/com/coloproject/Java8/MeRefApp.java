package com.coloproject.Java8;

import java.util.Arrays;

import com.coloproject.Java8.interfaces.IPersona;
import com.coloproject.Java8.interfaces.PersonaB;
import com.coloproject.Java8.model.Persona;

//Por el momento el referenciado de metodos no permite que tengan parametros de entrada.
public class MeRefApp {
	
	public static void referenciarMetodoStatic() {
		System.out.println("Metodo referido Static");
	}
	
	public void refrenciarMetodoInstanciaObjetoArbitrario() {
		String[] nombres = {"Mito","Code","Jaime"};
				
		/*Arrays.sort(nombres, new Comparator<String>(){
			@Override
			public int compare(String ol1 ,String ol2) {
				return ol1.compareTo(ol2);
			}
		});*/
		//Lo pasamos a Lambda
//		Arrays.sort(nombres,(String p1, String p2)-> p1.compareToIgnoreCase(p2));
//		System.out.println(Arrays.toString(nombres));
		
		//Referencia a metodo con lambdas
		Arrays.sort(nombres,String::compareToIgnoreCase);
		System.out.println(Arrays.toString(nombres));
	}
	
	public void referenciaMetodoInstanciaObjetoParticular() {
		System.out.println("Metodo referido instancia de un Objeto Particular");
	}
	
	public void referenciaConstructor() {

//		IPersona per = new IPersona() {
//			@Override
//			public Persona crear(int id, String nombre) {
//				return new Persona(id, nombre);
//			}
//		};
//		
//		per.crear(1, "Carlos");
//		IPersona iper2 = (x,y) -> new Persona(x, y);
//		Persona per2 = iper2.crear(1, "Arnoldo");
		
		IPersona iper3 = Persona::new;
		Persona per3 = iper3.crear(2, "Roberto");
		System.out.println(per3.getId() + " - " + per3.getNombre());
		
	}
	
	public void operar() {
//		PersonaB op = () -> MeRefApp.referenciarMetodoStatic();
//		op.saludar();
		PersonaB per = MeRefApp::referenciarMetodoStatic;
		per.saludar();
	}
	
	public static void main(String[] args) {
		MeRefApp app = new MeRefApp();
		app.operar();
		app.refrenciarMetodoInstanciaObjetoArbitrario();
		PersonaB per = app::referenciaMetodoInstanciaObjetoParticular;
		per.saludar();
	}
}

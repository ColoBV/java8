package com.coloproject.Java8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.coloproject.Java8.interfaces.Operaciones;

public class LamdaApp{

	public void ordernar() {
		List<String> lista = new ArrayList<>();
		lista.add("MitoCode");
		lista.add("Code");
		lista.add("Mito");
		
		/*Esta es la manera de hacerlo de manera declarativa y en 1.7
		 * Collections.sort(lista, new Comparator<String>(){
			
			@Override
			public int compare(String ol1,String ol2) {
				return ol1.compareTo(ol2);
			}
			
		});*/
		
		//Con expresiones lamda pasaria expresarse de la siguiente manera y resuelve exactamente lo mismo.
		Collections.sort(lista,(String p1, String p2)-> p1.compareTo(p2));
		
		for(String elemento : lista) {
			System.out.println(elemento);
		}
	}
	
	public void calcular() {
		/* Metodo declarativo.
		Operaciones operacion = new Operaciones() {
			
			@Override
			public double calcularPromedio(double n1,double n2) {
				return (n1+n2)/2;
			}
		};
		
		System.out.println(operacion.calcularPromedio(2, 3));*/
		
		//Declaracion de la misma operacion con Lamda
		//Se declara una instancia de un objeto que implementa la interfaz Operacion.
//		Operaciones operacion = (double x, double y) -> (x+y)/2;
//		System.out.println(operacion.calcularPromedio(5, 7));
	}
 
	public double probarSintaxis() {
//		Operaciones operacion = (double x, double y) -> (x+y)/2;
//		Operaciones operacion = (double x, double y) -> {return (x+y)/2;};
//		Se puede expresar sin parametros la expresion lamda. Pero tengo que tener en la interfaz el metodo sin parametros.
//		 Operaciones operacion = () -> 2;
		
//		Operaciones operacion = (x,y) -> {
//			double x = 2;
//			double y = 3;
//			return (x+y);
//		};
		
		Operaciones operacion = (double x, double y) -> {
			//Aca se puede hacer lo que querramos. Pero no es muy lindo y legible.
			double a = 2.0;
			System.out.println(a);
			return (x+y)/2 + a;
			
		};
		
		return operacion.calcularPromedio(3,6);
	}
	
	public static void main(String[] args) {
		LamdaApp app = new LamdaApp();
		app.ordernar();
		app.calcular();
		System.out.println(app.probarSintaxis());
	}

}
